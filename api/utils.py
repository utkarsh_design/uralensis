import sys
import psutil
from pathlib import Path
import os
import uuid
import datetime

from django.conf import settings


# Helper functions

def _get_folder_size_and_count(pth, extension=None):
    root_directory = Path(pth)
    sz = 0
    count = 0
    for f in root_directory.glob('**/*'):
        if f.is_file():
            sz += f.stat().st_size
            if extension is not None:
                if str(f).split('.')[-1] == extension:
                    count += 1
            else:
                count += 1
    return (sz, count)

def _get_folder_size_word(pth, sz = None):
    if sz is None:
        sz = _get_folder_size_and_count(pth)[0]
    if sz < 1024:
        return str(round(sz, 1))+' B'
    sz = sz / 1024
    if sz < 1024:
        return str(round(sz, 1))+ ' KB'
    sz = sz / 1024
    if sz < 1024:
        return str(round(sz, 1))+ ' MB'
    sz = sz / 1024
    return str(round(sz, 1))+ ' GB'

def _get_file_size(pth):
    full_path = Path(os.path.join(settings.SLIDES_DIR, pth))
    if full_path.is_file():
        sz = full_path.stat().st_size
        words = _get_folder_size_and_count('.', sz)
        return (sz, words)
    return None
    

def _get_unique_filename(fn):
    us_exts = fn.split('.')
    us_old_name = us_exts[0]
    unique_filename = str(uuid.uuid4())
    us_new_name = us_old_name+'__'+unique_filename
    us_exts[0] = us_new_name
    us_full_name = '.'.join(us_exts)
    return us_full_name

def _get_iso_date(dt):
    if len(dt) == 0:
        return str(datetime.date.today())
    d = dt.split('/')
    return str(datetime.date(int(d[2]), int(d[0]), int(d[1])))

def _is_valid_date(dt):
    if type(dt) != str:
        return False
    parts = dt.split('/')
    if len(parts) != 3:
        return False
    for p in parts:
        if not p.isnumeric():
            return False
    return True 