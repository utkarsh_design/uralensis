-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 22, 2021 at 04:54 PM
-- Server version: 5.7.36
-- PHP Version: 7.3.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wpuralen_slides_management`
--

-- --------------------------------------------------------

--
-- Table structure for table `auth_group`
--

CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_group_permissions`
--

CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_permission`
--

CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add log entry', 1, 'add_logentry'),
(2, 'Can change log entry', 1, 'change_logentry'),
(3, 'Can delete log entry', 1, 'delete_logentry'),
(4, 'Can view log entry', 1, 'view_logentry'),
(5, 'Can add permission', 2, 'add_permission'),
(6, 'Can change permission', 2, 'change_permission'),
(7, 'Can delete permission', 2, 'delete_permission'),
(8, 'Can view permission', 2, 'view_permission'),
(9, 'Can add group', 3, 'add_group'),
(10, 'Can change group', 3, 'change_group'),
(11, 'Can delete group', 3, 'delete_group'),
(12, 'Can view group', 3, 'view_group'),
(13, 'Can add user', 4, 'add_user'),
(14, 'Can change user', 4, 'change_user'),
(15, 'Can delete user', 4, 'delete_user'),
(16, 'Can view user', 4, 'view_user'),
(17, 'Can add content type', 5, 'add_contenttype'),
(18, 'Can change content type', 5, 'change_contenttype'),
(19, 'Can delete content type', 5, 'delete_contenttype'),
(20, 'Can view content type', 5, 'view_contenttype'),
(21, 'Can add session', 6, 'add_session'),
(22, 'Can change session', 6, 'change_session'),
(23, 'Can delete session', 6, 'delete_session'),
(24, 'Can view session', 6, 'view_session'),
(25, 'Can add folder', 7, 'add_folder'),
(26, 'Can change folder', 7, 'change_folder'),
(27, 'Can delete folder', 7, 'delete_folder'),
(28, 'Can view folder', 7, 'view_folder'),
(29, 'Can add slide', 8, 'add_slide'),
(30, 'Can change slide', 8, 'change_slide'),
(31, 'Can delete slide', 8, 'delete_slide'),
(32, 'Can view slide', 8, 'view_slide'),
(33, 'Can add annotation', 9, 'add_annotation'),
(34, 'Can change annotation', 9, 'change_annotation'),
(35, 'Can delete annotation', 9, 'delete_annotation'),
(36, 'Can view annotation', 9, 'view_annotation'),
(37, 'Can add activity', 10, 'add_activity'),
(38, 'Can change activity', 10, 'change_activity'),
(39, 'Can delete activity', 10, 'delete_activity'),
(40, 'Can view activity', 10, 'view_activity');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user`
--

CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(150) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `auth_user`
--

INSERT INTO `auth_user` (`id`, `password`, `last_login`, `is_superuser`, `username`, `first_name`, `last_name`, `email`, `is_staff`, `is_active`, `date_joined`) VALUES
(1, 'pbkdf2_sha256$260000$K5KDbSPv8J5gM0dMiA6e3f$A2CWxsh3JMghHfkH8KPUXbWU8Q44STBvhpCDt79pnBg=', '2021-11-18 11:15:15.506818', 1, 'admin', '', '', '', 1, 1, '2021-07-28 11:24:38.556527'),
(2, 'pbkdf2_sha256$260000$K5KDbSPv8J5gM0dMiA6e3f$A2CWxsh3JMghHfkH8KPUXbWU8Q44STBvhpCDt79pnBg=', '2021-10-11 18:44:11.946260', 1, 'admin2', '', '', '', 1, 1, '2021-07-28 11:24:38.556527');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_groups`
--

CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_user_permissions`
--

CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dashboard_activity`
--

CREATE TABLE `dashboard_activity` (
  `id` int(11) NOT NULL,
  `Saved` tinyint(1) NOT NULL,
  `LastAccessed` datetime(6) NOT NULL,
  `Slide_Id_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dashboard_annotation`
--

CREATE TABLE `dashboard_annotation` (
  `id` int(11) NOT NULL,
  `Json` longtext NOT NULL,
  `AnnotationText` longtext NOT NULL,
  `Type` varchar(10) NOT NULL,
  `Slide_Id_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dashboard_annotation`
--

INSERT INTO `dashboard_annotation` (`id`, `Json`, `AnnotationText`, `Type`, `Slide_Id_id`, `user_id`, `created_at`) VALUES
(1, '[\"Shape\",{\"applyMatrix\":false,\"matrix\":[1,0,0,1,14062.39571,1928.30138],\"type\":\"rectangle\",\"size\":[861.04905,373.46706],\"radius\":[0,0],\"fillColor\":[1,1,1,0.05],\"strokeColor\":[1,0,0],\"strokeWidth\":82.89525}]', '', 'r', 5, NULL, NULL),
(2, '[\"Path\",{\"applyMatrix\":true,\"segments\":[[18299.8858,4278.86457],[18320.63397,4237.36823]],\"fillColor\":[1,0,0],\"strokeColor\":[1,0,0],\"strokeWidth\":82.89525}]', '', 'l', 5, NULL, NULL),
(51, '[\"Path\",{\"applyMatrix\":true,\"segments\":[[10554.49172,8699.24948],[10790.40081,8713.34039]],\"fillColor\":[1,0,0],\"strokeColor\":[1,0,0],\"strokeWidth\":2.48673}]', '', 'l', 204, NULL, NULL),
(14, '[\"Path\",{\"applyMatrix\":true,\"segments\":[[14286.22286,2158.45872],[20496.07283,1951.97356]],\"fillColor\":[1,0,0],\"strokeColor\":[1,0,0],\"strokeWidth\":30.59039}]', '', 'l', 6, NULL, NULL),
(42, '[\"Path\",{\"applyMatrix\":true,\"segments\":[[2722.54506,2842.91878],[22820.43389,1986.38775]],\"fillColor\":[1,0,0],\"strokeColor\":[1,0,0],\"strokeWidth\":22.8945}]', '', 'l', 10, NULL, NULL),
(27, '[\"Path\",{\"applyMatrix\":true,\"segments\":[[24079.00419,3026.217],[24385.00419,3443.217]],\"fillColor\":[1,0,0],\"strokeColor\":[1,0,0],\"strokeWidth\":22.89355}]', '', 'l', 10, NULL, NULL),
(32, '[\"Shape\",{\"applyMatrix\":false,\"matrix\":[1,0,0,1,11676.73235,3422.77338],\"type\":\"circle\",\"size\":[6351.88014,6351.88014],\"radius\":3175.94007,\"fillColor\":[1,1,1,0.05],\"strokeColor\":[0,0,0],\"strokeWidth\":0.78663}]', '', 'c', 6, NULL, NULL),
(33, '[\"Shape\",{\"applyMatrix\":false,\"matrix\":[1,0,0,1,2922.93194,1626.52279],\"type\":\"circle\",\"size\":[1595.09165,1595.09165],\"radius\":797.54582,\"fillColor\":[1,1,1,0.05],\"strokeColor\":[0,0,0],\"strokeWidth\":49.14851}]', '', 'c', 6, NULL, NULL),
(34, '[\"Shape\",{\"applyMatrix\":false,\"matrix\":[1,0,0,1,22203.84405,2399.01208],\"type\":\"rectangle\",\"size\":[1586.73508,1524.09987],\"radius\":[0,0],\"fillColor\":[1,1,1,0.05],\"strokeColor\":[0,0,0],\"strokeWidth\":49.14851}]', '', 'r', 6, NULL, NULL),
(53, '[\"Shape\",{\"applyMatrix\":false,\"matrix\":[1,0,0,1,13960.98508,18383.27391],\"type\":\"rectangle\",\"size\":[3325.952,475.136],\"radius\":[0,0],\"fillColor\":[1,1,1,0.05],\"strokeColor\":[1,0,0],\"strokeWidth\":4.45352}]', '', 'r', 221, NULL, NULL),
(81, '[\"Path\",{\"applyMatrix\":true,\"segments\":[[103254.4982,78657.95588],[105257.04982,78637.14415]],\"fillColor\":[1,0,0],\"strokeColor\":[1,0,0],\"strokeWidth\":110.38948}]', '', 'l', 485, 1, '2021-11-01 14:56:18'),
(80, '[\"Path\",{\"applyMatrix\":true,\"segments\":[[101230.47638,78720.96083],[103259.73718,78707.34163]],\"fillColor\":[1,0,0],\"strokeColor\":[1,0,0],\"strokeWidth\":217.47014}]', 'Annotation 22', 'l', 485, 2, '2021-10-14 12:28:03'),
(83, '[\"Path\",{\"applyMatrix\":true,\"segments\":[[76959.74856,22224.47954],[77492.08188,22463.81287]],\"fillColor\":[1,0,0],\"strokeColor\":[1,0,0],\"strokeWidth\":72.68034}]', '', 'l', 426, 1, '2021-11-11 12:18:40'),
(84, '[\"Path\",{\"applyMatrix\":true,\"segments\":[[40743.23553,44845.47386],[46678.71897,49468.16814]],\"fillColor\":[1,0,0],\"strokeColor\":[1,0,0],\"strokeWidth\":159.29813}]', '', 'l', 474, 1, '2021-11-12 09:10:03');

-- --------------------------------------------------------

--
-- Table structure for table `dashboard_folder`
--

CREATE TABLE `dashboard_folder` (
  `id` int(11) NOT NULL,
  `Name` longtext NOT NULL,
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `Parent_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dashboard_folder`
--

INSERT INTO `dashboard_folder` (`id`, `Name`, `Created`, `Modified`, `Parent_id`) VALUES
(1, 'FTP', '2021-07-28', '2021-07-28', NULL),
(2, 'PH0002D', '2021-07-28', '2021-07-28', 1),
(3, 'PH0001D', '2021-07-30', '2021-07-30', 1),
(5, 'PH0003D', '2021-08-01', '2021-08-01', 1),
(6, 'PH0004D', '2021-08-02', '2021-08-02', 1),
(14, 'LDS 2021', '2021-09-09', '2021-09-09', 13),
(13, 'DermpathPRO', '2021-09-09', '2021-09-09', 1),
(44, 'Self Assessment Cases', '2021-09-15', '2021-09-15', 14),
(45, 'LDS Self Assessment Case 6', '2021-09-15', '2021-09-15', 44),
(19, 'Slide Library case 1', '2021-09-15', '2021-09-15', 14),
(20, 'Slide Library case 2', '2021-09-15', '2021-09-15', 14),
(161, 'Case006', '2021-09-20', '2021-09-20', 155),
(135, 'Slide Library case 12', '2021-09-16', '2021-09-16', 14),
(136, 'Slide Library case 13', '2021-09-16', '2021-09-16', 14),
(137, 'Slide Library case 14', '2021-09-16', '2021-09-16', 14),
(160, 'Case020', '2021-09-20', '2021-09-20', 155),
(56, 'Slide Library case 4', '2021-09-15', '2021-09-15', 14),
(57, 'Slide Library case 5', '2021-09-15', '2021-09-15', 14),
(55, 'LDS Self Assessment Case 20', '2021-09-15', '2021-09-15', 44),
(47, 'LDS Self Assessment Case 8', '2021-09-15', '2021-09-15', 44),
(48, 'LDS Self Assessment Case 10', '2021-09-15', '2021-09-15', 44),
(49, 'LDS Self Assessment Case 19', '2021-09-15', '2021-09-15', 44),
(50, 'LDS Self Assessment Case 16', '2021-09-15', '2021-09-15', 44),
(51, 'LDS Self Assessment Case 18', '2021-09-15', '2021-09-15', 44),
(52, 'LDS Self Assessment Case 7', '2021-09-15', '2021-09-15', 44),
(53, 'LDS Self Assessment Case 17', '2021-09-15', '2021-09-15', 44),
(54, 'LDS Self Assessment Case 9', '2021-09-15', '2021-09-15', 44),
(134, 'Slide Library case 11', '2021-09-16', '2021-09-16', 14),
(159, 'Case019', '2021-09-20', '2021-09-20', 155),
(158, 'Case014', '2021-09-20', '2021-09-20', 155),
(144, '03541', '2021-09-18', '2021-09-18', 261),
(143, '36886 (2)', '2021-09-18', '2021-09-18', 261),
(157, 'Case001', '2021-09-20', '2021-09-20', 155),
(156, 'Case012', '2021-09-20', '2021-09-20', 155),
(155, 'FRCPath Part 2', '2021-09-20', '2021-09-20', 1),
(154, '39252', '2021-09-19', '2021-09-19', 261),
(274, '38679', '2021-09-23', '2021-09-23', 1),
(145, '36168', '2021-09-18', '2021-09-18', 261),
(152, '36886 (1)', '2021-09-19', '2021-09-19', 261),
(153, '36886 (3)', '2021-09-19', '2021-09-19', 261),
(258, 'Case 13', '2021-09-20', '2021-09-20', 165),
(182, 'Case009', '2021-09-20', '2021-09-20', 165),
(193, 'Case 15', '2021-09-20', '2021-09-20', 165),
(263, '39252', '2021-09-22', '2021-09-22', 1),
(196, 'Case 08', '2021-09-20', '2021-09-20', 165),
(168, 'Case007', '2021-09-20', '2021-09-20', 165),
(167, 'Case006', '2021-09-20', '2021-09-20', 165),
(166, 'Case005', '2021-09-20', '2021-09-20', 165),
(165, 'FRCPath P2 Mock', '2021-09-20', '2021-09-20', 1),
(164, 'Case003', '2021-09-20', '2021-09-20', 155),
(163, 'Case002', '2021-09-20', '2021-09-20', 155),
(162, 'Case008', '2021-09-20', '2021-09-20', 155),
(275, '38679', '2021-09-23', '2021-09-23', 13),
(273, 'TestCase', '2021-09-23', '2021-09-23', 1),
(272, 'TestCase', '2021-09-23', '2021-09-23', 1),
(271, 'TestCase', '2021-09-23', '2021-09-23', 1),
(276, 'testing file', '2021-09-24', '2021-09-24', 1),
(269, 'TestCase', '2021-09-23', '2021-09-23', 1),
(255, 'Case017', '2021-09-20', '2021-09-20', 246),
(257, 'Case018', '2021-09-20', '2021-09-20', 246),
(261, 'Spot Diagnosis', '2021-09-22', '2021-09-22', 13),
(248, 'Case011', '2021-09-20', '2021-09-20', 246),
(247, 'Case 09', '2021-09-20', '2021-09-20', 165),
(246, 'New FRCPath P2', '2021-09-20', '2021-09-20', 1),
(245, 'Case004', '2021-09-20', '2021-09-20', 246),
(277, 'Testing_repetition', '2021-09-24', '2021-09-24', 1),
(278, 'TestCase', '2021-09-24', '2021-09-24', 277),
(279, '50756', '2021-10-04', '2021-10-04', 261),
(280, 'QA-Measurements', '2021-10-07', '2021-10-07', 1),
(281, 'Measure_2021', '2021-10-07', '2021-10-07', 280),
(282, 'Dicom', '2021-10-11', '2021-10-11', 1);

-- --------------------------------------------------------

--
-- Table structure for table `dashboard_sharedannotation`
--

CREATE TABLE `dashboard_sharedannotation` (
  `id` int(11) NOT NULL,
  `Annotation_Id` int(11) NOT NULL,
  `Slide_Id_id` int(11) NOT NULL,
  `shared_from_user_id` int(11) DEFAULT NULL,
  `shared_from_user_name` varchar(255) DEFAULT NULL,
  `shared_to_user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dashboard_sharedannotation`
--

INSERT INTO `dashboard_sharedannotation` (`id`, `Annotation_Id`, `Slide_Id_id`, `shared_from_user_id`, `shared_from_user_name`, `shared_to_user_id`, `created_at`) VALUES
(14, 80, 485, 2, 'admin2', 1, '2021-10-14 12:28:17');

-- --------------------------------------------------------

--
-- Table structure for table `dashboard_slide`
--

CREATE TABLE `dashboard_slide` (
  `id` int(11) NOT NULL,
  `Name` mediumtext NOT NULL,
  `ScannedBy` varchar(50) NOT NULL,
  `ScannedDate` date NOT NULL,
  `InsertedBy` varchar(50) NOT NULL,
  `InsertedDate` date NOT NULL,
  `SlideType` int(11) NOT NULL,
  `UrlPath` varchar(500) NOT NULL,
  `LabelUrlPath` varchar(500) NOT NULL,
  `Group` int(11) NOT NULL,
  `GroupName` varchar(100) NOT NULL,
  `Annotations` tinyint(1) NOT NULL,
  `Folder_id` int(11) NOT NULL,
  `Filesize` double NOT NULL,
  `Tag` varchar(50) NOT NULL,
  `CopyOf_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dashboard_slide`
--

INSERT INTO `dashboard_slide` (`id`, `Name`, `ScannedBy`, `ScannedDate`, `InsertedBy`, `InsertedDate`, `SlideType`, `UrlPath`, `LabelUrlPath`, `Group`, `GroupName`, `Annotations`, `Folder_id`, `Filesize`, `Tag`, `CopyOf_id`) VALUES
(475, '38679', 'FTP', '2021-09-23', 'FTP', '2021-09-23', 2, '23-09-2021/38679.mrxs', '/static/images/placeholder.png', 0, 'Default Group', 1, 275, 62113, '', NULL),
(2, '19HAMA0001SMITHA1HE_-_2020-07-30_16.06.17.ndpi', 'FTP', '2021-07-28', 'FTP', '2021-07-28', 2, '28-07-2021/19HAMA0001SMITHA1HE_-_2020-07-30_16.06.17.ndpi', '/static/images/placeholder.png', 0, 'Default Group', 1, 2, 355269672, '', NULL),
(3, '00EP1.svs', 'FTP', '2021-07-28', 'FTP', '2021-07-28', 2, '28-07-2021/00EP1.svs', '/static/images/placeholder.png', 0, 'Default Group', 1, 2, 872637465, '', NULL),
(4, 'PAS_stain.svs', 'FTP', '2021-07-28', 'FTP', '2021-07-28', 2, '28-07-2021/PAS_stain.svs', '/static/images/placeholder.png', 0, 'Default Group', 1, 2, 12572539, '', NULL),
(5, 'PAS_stain.svs', 'FTP', '2021-07-30', 'FTP', '2021-07-30', 2, '30-07-2021/PAS_stain.svs', '/static/images/placeholder.png', 0, 'Default Group', 1, 3, 12572539, '', NULL),
(6, 'PAS.svs', 'FTP', '2021-07-30', 'FTP', '2021-07-30', 2, '30-07-2021/PAS.svs', '/static/images/labels/label__2af1fbbc-af53-4446-bd87-811a6d724a5e.png', 0, 'Default Group', 1, 3, 12572539, '', NULL),
(8, 'PAS_stain.svs', 'FTP', '2021-08-01', 'FTP', '2021-08-01', 2, '01-08-2021/PAS_stain.svs', '/static/images/placeholder.png', 0, 'Default Group', 1, 5, 4456380, '', NULL),
(9, 'PAS_stain.svs', 'FTP', '2021-08-01', 'FTP', '2021-08-01', 2, '01-08-2021/PAS_stain.svs', '/static/images/placeholder.png', 0, 'Default Group', 1, 5, 4456380, '', NULL),
(10, 'PAS_stain.svs', 'FTP', '2021-08-02', 'FTP', '2021-08-02', 2, '02-08-2021/PAS_stain.svs', '/static/images/placeholder.png', 0, 'Default Group', 1, 6, 12572539, '', NULL),
(327, 'Case020.mrxs', 'FTP', '2021-09-20', 'FTP', '2021-09-20', 2, '20-09-2021/Case020.mrxs', '/static/images/placeholder.png', 0, 'Default Group', 1, 160, 7783, '', NULL),
(328, 'Case006.mrxs', 'FTP', '2021-09-20', 'FTP', '2021-09-20', 2, '20-09-2021/Case006.mrxs', '/static/images/placeholder.png', 0, 'Default Group', 1, 161, 16592, '', NULL),
(329, 'Case008.mrxs', 'FTP', '2021-09-20', 'FTP', '2021-09-20', 2, '20-09-2021/Case008.mrxs', '/static/images/placeholder.png', 0, 'Default Group', 1, 162, 14479, '', NULL),
(330, 'Case002.mrxs', 'FTP', '2021-09-20', 'FTP', '2021-09-20', 2, '20-09-2021/Case002.mrxs', '/static/images/placeholder.png', 0, 'Default Group', 1, 163, 8529, '', NULL),
(331, 'Case003.mrxs', 'FTP', '2021-09-20', 'FTP', '2021-09-20', 2, '20-09-2021/Case003.mrxs', '/static/images/placeholder.png', 0, 'Default Group', 1, 164, 13381, '', NULL),
(458, '39252', 'FTP', '2021-09-22', 'FTP', '2021-09-22', 2, '22-09-2021/39252.mrxs', '/static/images/placeholder.png', 0, 'Default Group', 1, 154, 7510, '', NULL),
(459, '39252', 'FTP', '2021-09-22', 'FTP', '2021-09-22', 2, '22-09-2021/39252.mrxs', '/static/images/placeholder.png', 0, 'Default Group', 1, 263, 7510, '', NULL),
(333, 'Case005', 'FTP', '2021-09-20', 'FTP', '2021-09-20', 2, '20-09-2021/Case005.mrxs', '/static/images/placeholder.png', 0, 'Default Group', 1, 166, 9398, '', NULL),
(204, 'LDS Self Assessment Case 16', 'FTP', '2021-09-15', 'FTP', '2021-09-15', 2, '15-09-2021/16_Lehman_Black Dot Poison Ivy__d6968444-6806-45c2-b57b-c9505af20123.svs', '/static/images/placeholder.png', 0, 'Default Group', 1, 50, 101787299, '', NULL),
(205, 'Slide Library case 3', 'IHC', '2021-09-15', 'IHC', '2021-09-15', 2, '15-09-2021/H21,24525,A1,KEVAN__20210628_163628__2440964e-50e9-4529-b8a1-999b113f1c43.tiff', '/static/images/placeholder.png', 0, 'Default Group', 0, 14, 1800761410, '', NULL),
(179, 'LDS Self Assessment Case 7', 'FTP', '2021-09-15', 'FTP', '2021-09-15', 2, '15-09-2021/Case 7__74663f6c-1be7-47d7-a955-c0d6442a9d2d.svs', '/static/images/placeholder.png', 0, 'Default Group', 1, 52, 311966738, '', NULL),
(334, 'Case006', 'FTP', '2021-09-20', 'FTP', '2021-09-20', 2, '20-09-2021/Case006.mrxs', '/static/images/placeholder.png', 0, 'Default Group', 1, 167, 16592, '', NULL),
(181, 'LDS Self Assessment Case 9', 'FTP', '2021-09-15', 'FTP', '2021-09-15', 2, '15-09-2021/Case 9__e3bbb653-2857-47cd-ba23-469aaf669bdf.svs', '/static/images/placeholder.png', 0, 'Default Group', 1, 54, 161758572, '', NULL),
(183, 'LDS Self Assessment Case 17', 'FTP', '2021-09-15', 'FTP', '2021-09-15', 2, '15-09-2021/LDS_Self_Assessment_Case_17.svs', '/static/images/placeholder.png', 0, 'Default Group', 1, 53, 1022141115, '', NULL),
(310, '36168 (1)', 'FTP', '2021-09-18', 'FTP', '2021-09-18', 2, '18-09-2021/36168 (1).mrxs', '/static/images/labels/Screenshot_5__62148e55-28e1-4bc0-918a-8a2c472059d2.jpg', 0, 'Default Group', 1, 145, 81870, '', NULL),
(309, '03541.mrxs', 'FTP', '2021-09-18', 'FTP', '2021-09-18', 2, '18-09-2021/03541.mrxs', '/static/images/labels/label__80f86ae4-5432-4db7-a1ca-315a6624b9fc.png', 0, 'Default Group', 1, 261, 10045, '', NULL),
(308, '36886 (2)', 'FTP', '2021-09-18', 'FTP', '2021-09-18', 2, '18-09-2021/36886 (2).mrxs', '/static/images/placeholder.png', 0, 'Default Group', 1, 143, 7472, '', NULL),
(292, 'Case 11', 'IHC', '2021-09-17', 'IHC', '2021-09-17', 2, '16-09-2021/H21,14753,A1,COSTELLO,HE_HE_20210423_123448__e89d2131-e47a-4957-8e32-e6c7e447bc41.tiff', '/static/images/labels/Case 11__ba5f2e79-d45c-49fa-9409-c1b1cd62fd48.jpg', 0, 'Default Group', 1, 134, 777977609, '', NULL),
(293, 'Case 11 Ki67', 'IHC', '2021-09-17', 'IHC', '2021-09-17', 2, '16-09-2021/H21,14753,A1,COSTELLO,KI67_KI67_20210429_120319__0d9f3410-fb54-4b7e-8b5c-97e38dab8347.tiff', '/static/images/labels/Case 11-Ki67__e39e9f3c-8788-4f97-bf5d-0658fe8d16a1.jpg', 0, 'Default Group', 1, 134, 1064301234, '', NULL),
(294, 'Case 11 MLH1', 'IHC', '2021-09-17', 'IHC', '2021-09-17', 2, '16-09-2021/H21,14753,A1,COSTELLO,MLH1_MLH1_20210429_120007__d66f6abb-9226-4192-8297-30f915f0b6a1.tiff', '/static/images/labels/Case 11-MLH__4481d144-a6e8-426a-9a9b-eab66e92a279.jpg', 0, 'Default Group', 1, 134, 738212836, '', NULL),
(295, 'Case 11 PMS2', 'IHC', '2021-09-17', 'IHC', '2021-09-17', 2, '16-09-2021/H21,14753,A1,COSTELLO,PMS2_PMS2_20210429_115407_(3)__0cf54f0a-261b-497b-8a69-331f73448b5f.tiff', '/static/images/labels/Case 11-PMS2__117c01b8-5825-438d-8026-57a17b2b27f4.jpg', 0, 'Default Group', 1, 134, 733208692, '', NULL),
(429, 'Case011.mrxs', 'FTP', '2021-09-20', 'FTP', '2021-09-20', 2, '20-09-2021/Case011.mrxs', '/static/images/placeholder.png', 0, 'Default Group', 1, 248, 13238, '', NULL),
(474, '38679', 'FTP', '2021-09-23', 'FTP', '2021-09-23', 2, '23-09-2021/38679.mrxs', '/static/images/placeholder.png', 0, 'Default Group', 1, 274, 62113, '', NULL),
(471, 'TestCase', 'FTP', '2021-09-23', 'FTP', '2021-09-23', 2, '23-09-2021/TestCase.mrxs', '/static/images/placeholder.png', 0, 'Default Group', 1, 271, 13238, '', NULL),
(476, '39252.mrxs', 'FTP', '2021-09-24', 'FTP', '2021-09-24', 2, '24-09-2021/39252.mrxs', '/static/images/placeholder.png', 0, 'Default Group', 1, 276, 0, '', NULL),
(439, 'Case017', 'FTP', '2021-09-20', 'FTP', '2021-09-20', 2, '20-09-2021/Case017.mrxs', '/static/images/placeholder.png', 0, 'Default Group', 1, 255, 7520, '', NULL),
(461, '39252.mrxs', 'FTP', '2021-09-22', 'FTP', '2021-09-22', 2, '22-09-2021/39252.mrxs', '/static/images/placeholder.png', 0, 'Default Group', 1, 263, 7510, '', NULL),
(477, 'nesxttest.mrxs', 'FTP', '2021-09-24', 'FTP', '2021-09-24', 2, '24-09-2021/nesxttest.mrxs', '/static/images/placeholder.png', 0, 'Default Group', 1, 276, 0, '', NULL),
(468, 'TestCase', 'FTP', '2021-09-23', 'FTP', '2021-09-23', 2, '23-09-2021/TestCase.mrxs', '/static/images/placeholder.png', 0, 'Default Group', 1, 269, 13238, '', NULL),
(340, 'Case007', 'FTP', '2021-09-20', 'FTP', '2021-09-20', 2, '20-09-2021/Case007.mrxs', '/static/images/placeholder.png', 0, 'Default Group', 1, 168, 9307, '', NULL),
(128, 'Case 1 - Immuno', 'IHC', '2021-09-15', 'IHC', '2021-09-15', 2, '15-09-2021/H21,20500,A1,DANCE,BAP1_BAP1_20210629_121326__c40f311e-731d-44c0-bd11-671bcc4ffbda.tiff', '/static/images/placeholder.png', 0, 'Default Group', 1, 19, 76926497, '', NULL),
(182, 'LDS Self Assessment Case 6', 'FTP', '2021-09-15', 'FTP', '2021-09-15', 2, '15-09-2021/Case 6__605baf40-4e1f-495e-b702-e3550ea079f8.svs', '/static/images/placeholder.png', 0, 'Default Group', 1, 45, 557558382, '', NULL),
(210, 'Case 05', 'IHC', '2021-09-15', 'IHC', '2021-09-15', 2, '15-09-2021/H21,18962,B,BEACH__20210521_132623__6b571a02-11b3-4904-bab5-095471718f18.tiff', '/static/images/labels/Case 1__54c78297-cb85-40f3-b7ec-be799259616f.jpg', 0, 'Default Group', 0, 57, 834540419, '', NULL),
(180, 'LDS Self Assessment Case 8', 'FTP', '2021-09-15', 'FTP', '2021-09-15', 2, '15-09-2021/Case 8__fbec17ed-f0bc-490a-81bf-9ee97ae03c9c.svs', '/static/images/placeholder.png', 0, 'Default Group', 1, 47, 322064548, '', NULL),
(203, 'LDS Self Assessment Case 20', 'FTP', '2021-09-15', 'FTP', '2021-09-15', 2, '15-09-2021/LDS_Self_Assessment_Case20.svs', '/static/images/placeholder.png', 0, 'Default Group', 1, 55, 458147395, '', NULL),
(177, 'LDS Self Assessment Case 10', 'FTP', '2021-09-15', 'FTP', '2021-09-15', 2, '15-09-2021/Case 10__9fa6b408-5e6b-4c62-b648-2bf7f48cfd53.svs', '/static/images/placeholder.png', 0, 'Default Group', 1, 48, 623058412, '', NULL),
(202, 'LDS Self Assessment Case 19', 'FTP', '2021-09-15', 'FTP', '2021-09-15', 2, '15-09-2021/19_Lehman_Metastatic gastric CA__66092c74-0457-485d-9b49-6ae7d3cee375.svs', '/static/images/placeholder.png', 0, 'Default Group', 1, 49, 446023607, '', NULL),
(206, 'Case 2', 'IHC', '2021-09-15', 'IHC', '2021-09-15', 2, '15-09-2021/H21,27080,,SMALLWOO,HE0-2_HE0-2_20210713_172247__4d6ac434-41ee-4f36-945e-5846d5e3f20b.tiff', '/static/images/labels/Case 1__54c78297-cb85-40f3-b7ec-be799259616f.jpg', 0, 'Default Group', 1, 20, 221546868, '', NULL),
(208, 'Case 4', 'IHC', '2021-09-15', 'IHC', '2021-09-15', 2, '15-09-2021/H21,21106,A2,OLD,HE_HE_20210604_144959__3d9192ec-64c0-4783-95e3-60bf7780b186.tiff', '/static/images/labels/Case 1__54c78297-cb85-40f3-b7ec-be799259616f.jpg', 0, 'Default Group', 0, 56, 1434419036, '', NULL),
(190, 'LDS Self Assessment Case 18', 'FTP', '2021-09-15', 'FTP', '2021-09-15', 2, '15-09-2021/LDS_Self_Assessment_Case_18.svs', '/static/images/placeholder.png', 0, 'Default Group', 1, 51, 1877750021, '', NULL),
(221, 'Case 1', 'IHC', '2021-09-16', 'IHC', '2021-09-16', 2, '16-09-2021/H21,20500,,DANCE,HE_HE_20210629_121245__0ce1861f-1e56-4d0f-9785-95b3c46cf57f.tiff', '/static/images/labels/Case 1__54c78297-cb85-40f3-b7ec-be799259616f.jpg', 0, 'Default Group', 1, 19, 80918101, '', NULL),
(472, 'TestCase', 'FTP', '2021-09-23', 'FTP', '2021-09-23', 2, '23-09-2021/TestCase.mrxs', '/static/images/placeholder.png', 0, 'Default Group', 1, 272, 13238, '', NULL),
(473, 'TestCase', 'FTP', '2021-09-23', 'FTP', '2021-09-23', 2, '23-09-2021/TestCase.mrxs', '/static/images/placeholder.png', 0, 'Default Group', 1, 273, 13238, '', NULL),
(445, 'Case018', 'FTP', '2021-09-20', 'FTP', '2021-09-20', 2, '20-09-2021/Case018.mrxs', '/static/images/placeholder.png', 0, 'Default Group', 1, 257, 185538, '', NULL),
(326, 'Case019.mrxs', 'FTP', '2021-09-20', 'FTP', '2021-09-20', 2, '20-09-2021/Case019.mrxs', '/static/images/placeholder.png', 0, 'Default Group', 1, 159, 11654, '', NULL),
(325, 'Case014.mrxs', 'FTP', '2021-09-20', 'FTP', '2021-09-20', 2, '20-09-2021/Case014.mrxs', '/static/images/placeholder.png', 0, 'Default Group', 1, 158, 8412, '', NULL),
(320, '36886 (3)', 'FTP', '2021-09-19', 'FTP', '2021-09-19', 2, '19-09-2021/36886 (3).mrxs', '/static/images/placeholder.png', 0, 'Default Group', 1, 153, 63745, '', NULL),
(315, '36168 (2)', 'FTP', '2021-09-18', 'FTP', '2021-09-18', 2, '18-09-2021/36168 (2).mrxs', '/static/images/labels/Screenshot_5__b81307c9-beee-4da1-8d66-0ac6f69d4b32.jpg', 0, 'Default Group', 1, 145, 56367, '', NULL),
(323, 'Case012.mrxs', 'FTP', '2021-09-20', 'FTP', '2021-09-20', 2, '20-09-2021/Case012.mrxs', '/static/images/placeholder.png', 0, 'Default Group', 1, 156, 7587, '', NULL),
(324, 'Case001.mrxs', 'FTP', '2021-09-20', 'FTP', '2021-09-20', 2, '20-09-2021/Case001.mrxs', '/static/images/placeholder.png', 0, 'Default Group', 1, 157, 16243, '', NULL),
(480, '38679.mrxs', 'FTP', '2021-09-25', 'FTP', '2021-09-25', 2, '25-09-2021/38679.mrxs', '/static/images/placeholder.png', 0, 'Default Group', 1, 275, 62113, '', NULL),
(319, '36886 (1)', 'FTP', '2021-09-19', 'FTP', '2021-09-19', 2, '19-09-2021/36886 (1).mrxs', '/static/images/placeholder.png', 0, 'Default Group', 1, 152, 7768, '', NULL),
(302, 'Case 11 MSH6', 'IHC', '2021-09-17', 'IHC', '2021-09-17', 2, '17-09-2021/H21,14753,A1,COSTELLO,MSH6_MSH6_20210429_115546__3f5e7a67-2912-46f3-8033-1e1f320c63e6.tiff', '/static/images/labels/Case 11-MSH6__974d508b-b653-4275-9a23-b0594f435412.jpg', 0, 'Default Group', 1, 134, 538275212, '', NULL),
(301, 'Case 11 MSH2', 'IHC', '2021-09-17', 'IHC', '2021-09-17', 2, '17-09-2021/H21,14753,A1,COSTELLO,MSH2_MSH2_20210429_115747__7f784255-d852-4e65-b47a-af5ab04e9351.tiff', '/static/images/labels/Case 11-MSH2__d6f8ae13-0f7b-459f-9063-2ed48fac2fce.jpg', 0, 'Default Group', 1, 134, 621179288, '', NULL),
(300, 'Case 14 VK', 'IHC', '2021-09-17', 'IHC', '2021-09-17', 2, '17-09-2021/H21,25786,,LEECH,VON_KOSSA_VON_KOSSA_20210715_100723__c973416d-0f03-47ba-8c9d-90a486467076.tiff', '/static/images/labels/Case 14 VK__91ee70fb-ec54-48bc-89ef-88bc69cf542d.jpg', 0, 'Default Group', 1, 137, 972814690, '', NULL),
(299, 'Case 14', 'IHC', '2021-09-17', 'IHC', '2021-09-17', 2, '17-09-2021/H21,25786,,LEECH,HE1_HE1_20210705_151556__95080e77-a0f2-497a-8079-7a2837af24ec.tiff', '/static/images/labels/Case 14__0b0a0d59-283b-4dc1-b3f0-0cda174e5511.jpg', 0, 'Default Group', 1, 137, 670726396, '', NULL),
(298, 'Case 13 ', 'IHC', '2021-09-17', 'IHC', '2021-09-17', 2, '17-09-2021/H21,05332,,MAGEE,HE0-2_HE0-2_20210218_091452__6eaa9686-cf5d-4994-ac9e-8bf5fdd3ff64.tiff', '/static/images/labels/Case 13__0e33e32a-bc54-4fb8-a1f4-90066ad9c87e.jpg', 0, 'Default Group', 1, 136, 518659709, '', NULL),
(297, 'Case 12 Immuno', 'IHC', '2021-09-17', 'IHC', '2021-09-17', 2, '17-09-2021/H21,15572,A1,MOHAMMED,HHV8_HHV8_20210906_124258__a8c7aec3-d440-4e66-a123-b9571f50f74d.tiff', '/static/images/labels/Case 12 HHV8__143c9063-f524-4415-8179-2c92b35563c9.jpg', 0, 'Default Group', 1, 135, 178269790, '', NULL),
(296, 'Case 12 ', 'IHC', '2021-09-17', 'IHC', '2021-09-17', 2, '17-09-2021/H21,15572,,MOHHAMME,HE0-2_HE0-2_20210428_141504__19dd39ec-8d21-4e4c-b283-6f21312f65ff.tiff', '/static/images/labels/Case 12__74b1ccbb-8700-48de-a061-8beecc23ada6.jpg', 0, 'Default Group', 1, 135, 594558491, '', NULL),
(460, '39252.mrxs', 'FTP', '2021-09-22', 'FTP', '2021-09-22', 2, '22-09-2021/39252.mrxs', '/static/images/placeholder.png', 0, 'Default Group', 1, 263, 7510, '', NULL),
(451, 'Case 13 ', 'IHC', '2021-09-20', 'IHC', '2021-09-20', 2, '20-09-2021/H21,25786,,LEECH,HE1_HE1_20210705_151556__899f8b21-3f61-4339-9c6a-e99aef2ed9a8.tiff', '/static/images/labels/Case 13__83856f2e-725a-4256-8e79-a08e9fad8160.jpg', 0, 'Default Group', 1, 258, 670726396, '', NULL),
(399, 'Case 15', 'IHC', '2021-09-20', 'IHC', '2021-09-20', 2, '20-09-2021/H21,15572,,MOHHAMME,HE0-2_HE0-2_20210428_141504__ddbd4afa-5426-48e1-91db-3b72f2ae332c.tiff', '/static/images/labels/Case 15__f6aa6dd4-7706-4f9f-8b08-8f31c8565687.jpg', 0, 'Default Group', 1, 193, 594558491, '', NULL),
(366, 'Case009', 'FTP', '2021-09-20', 'FTP', '2021-09-20', 2, '20-09-2021/Case009.mrxs', '/static/images/placeholder.png', 0, 'Default Group', 1, 182, 10417, '', NULL),
(428, 'Case004', 'FTP', '2021-09-20', 'FTP', '2021-09-20', 2, '20-09-2021/Case004.mrxs', '/static/images/placeholder.png', 0, 'Default Group', 1, 245, 16059, '', NULL),
(426, 'Case 8', 'IHC', '2021-09-20', 'IHC', '2021-09-20', 2, '20-09-2021/H21,25786,,LEECH,HE1_HE1_20210705_151556__5031a3d6-ab12-4b29-9510-7f9ab32078cf.tiff', '/static/images/labels/Case 08__50dd5a66-b1dc-475e-8730-20534904877d.jpg', 0, 'Default Group', 1, 196, 670726396, '', NULL),
(478, 'PAS_stain__1fa272e5-45f2-4a18-a653-c0525a7859ad.svs', 'FTP', '2021-09-24', 'FTP', '2021-09-24', 2, '24-09-2021/PAS_stain__1fa272e5-45f2-4a18-a653-c0525a7859ad.svs', '/static/images/placeholder.png', 0, 'Default Group', 1, 276, 12572539, '', NULL),
(479, 'TestCase.mrxs', 'FTP', '2021-09-24', 'FTP', '2021-09-24', 2, '24-09-2021/TestCase.mrxs', '/static/images/labels/testingla__76e67020-fda3-458a-b550-5f045c09aa4a.png', 0, 'Default Group', 1, 278, 13238, '', NULL),
(481, '38679.mrxs', 'FTP', '2021-09-25', 'FTP', '2021-09-25', 2, '25-09-2021/38679.mrxs', '/static/images/placeholder.png', 0, 'Default Group', 1, 275, 62113, '', NULL),
(482, '50756.mrxs', 'FTP', '2021-10-04', 'FTP', '2021-10-04', 2, '04-10-2021/50756.mrxs', '/static/images/labels/Screenshot_5__611ec74f-8e61-4f83-b900-174172e7d6fa.jpg', 0, 'Default Group', 1, 279, 10276, '', NULL),
(485, 'Measure_2021.mrxs', 'FTP', '2021-10-07', 'FTP', '2021-10-07', 2, '07-10-2021/Measure_2021.mrxs', '/static/images/placeholder.png', 0, 'Default Group', 1, 281, 26046, '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `django_admin_log`
--

CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) UNSIGNED NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `django_content_type`
--

CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES
(1, 'admin', 'logentry'),
(2, 'auth', 'permission'),
(3, 'auth', 'group'),
(4, 'auth', 'user'),
(5, 'contenttypes', 'contenttype'),
(6, 'sessions', 'session'),
(7, 'dashboard', 'folder'),
(8, 'dashboard', 'slide'),
(9, 'dashboard', 'annotation'),
(10, 'dashboard', 'activity');

-- --------------------------------------------------------

--
-- Table structure for table `django_migrations`
--

CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `django_migrations`
--

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
(1, 'contenttypes', '0001_initial', '2021-07-28 11:18:10.157920'),
(2, 'auth', '0001_initial', '2021-07-28 11:18:11.106086'),
(3, 'admin', '0001_initial', '2021-07-28 11:18:11.282276'),
(4, 'admin', '0002_logentry_remove_auto_add', '2021-07-28 11:18:11.303606'),
(5, 'admin', '0003_logentry_add_action_flag_choices', '2021-07-28 11:18:11.309290'),
(6, 'contenttypes', '0002_remove_content_type_name', '2021-07-28 11:18:11.407912'),
(7, 'auth', '0002_alter_permission_name_max_length', '2021-07-28 11:18:11.524905'),
(8, 'auth', '0003_alter_user_email_max_length', '2021-07-28 11:18:11.575384'),
(9, 'auth', '0004_alter_user_username_opts', '2021-07-28 11:18:11.581267'),
(10, 'auth', '0005_alter_user_last_login_null', '2021-07-28 11:18:11.650797'),
(11, 'auth', '0006_require_contenttypes_0002', '2021-07-28 11:18:11.651520'),
(12, 'auth', '0007_alter_validators_add_error_messages', '2021-07-28 11:18:11.657164'),
(13, 'auth', '0008_alter_user_username_max_length', '2021-07-28 11:18:11.701510'),
(14, 'auth', '0009_alter_user_last_name_max_length', '2021-07-28 11:18:11.751749'),
(15, 'auth', '0010_alter_group_name_max_length', '2021-07-28 11:18:11.793539'),
(16, 'auth', '0011_update_proxy_permissions', '2021-07-28 11:18:11.799130'),
(17, 'auth', '0012_alter_user_first_name_max_length', '2021-07-28 11:18:11.835603'),
(18, 'dashboard', '0001_initial', '2021-07-28 11:18:12.197304'),
(19, 'dashboard', '0002_folder_parent', '2021-07-28 11:18:12.331427'),
(20, 'dashboard', '0003_remove_folder_path', '2021-07-28 11:18:12.365036'),
(21, 'dashboard', '0004_auto_20210227_1548', '2021-07-28 11:18:12.466104'),
(22, 'dashboard', '0005_auto_20210227_1634', '2021-07-28 11:18:12.470058'),
(23, 'dashboard', '0006_auto_20210301_1528', '2021-07-28 11:18:12.473552'),
(24, 'dashboard', '0007_auto_20210301_1543', '2021-07-28 11:18:12.575374'),
(25, 'dashboard', '0008_slide_tag', '2021-07-28 11:18:12.668802'),
(26, 'dashboard', '0009_slide_copyof', '2021-07-28 11:18:12.831545'),
(27, 'sessions', '0001_initial', '2021-07-28 11:18:12.903899');

-- --------------------------------------------------------

--
-- Table structure for table `django_session`
--

CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `django_session`
--

INSERT INTO `django_session` (`session_key`, `session_data`, `expire_date`) VALUES
('5sx8n3xvw2lqgausz010t42rloudeanj', '.eJxVjEEOwiAQRe_C2pBCYYa6dO8ZyMBMpWpoUtqV8e7apAvd_vfef6lI21ri1mSJE6uzMur0uyXKD6k74DvV26zzXNdlSnpX9EGbvs4sz8vh_h0UauVbi_PedRggIEDqE3QmoVhvM2DAYQzZjWhYMDAxWSPiHXLqwXG2gIN6fwC_zjdl:1m8hgX:GjUTyakqtD41BFxOTDOkFHosCbBxsDaXGJ7jYO3gtGI', '2021-08-11 11:25:13.955578'),
('qhc23it3uhm0wdv2rpb1m5o73q7dosjp', '.eJxVjEEOwiAQRe_C2pBCYYa6dO8ZyMBMpWpoUtqV8e7apAvd_vfef6lI21ri1mSJE6uzMur0uyXKD6k74DvV26zzXNdlSnpX9EGbvs4sz8vh_h0UauVbi_PedRggIEDqE3QmoVhvM2DAYQzZjWhYMDAxWSPiHXLqwXG2gIN6fwC_zjdl:1mB34D:b_OF8XkWDp8Bzeto_l_KDpaObsAe4xsoV7ZwQHIb3lk', '2021-08-17 22:39:21.117132'),
('alygh0tzq2r4l522owi7ayd4so78h7ex', '.eJxVjEEOwiAQRe_C2pBCYYa6dO8ZyMBMpWpoUtqV8e7apAvd_vfef6lI21ri1mSJE6uzMur0uyXKD6k74DvV26zzXNdlSnpX9EGbvs4sz8vh_h0UauVbi_PedRggIEDqE3QmoVhvM2DAYQzZjWhYMDAxWSPiHXLqwXG2gIN6fwC_zjdl:1mI8jg:iOLzJF1n0PFyy3AXjSzEQFfKoHAu6MpAL4tTnxOeYDM', '2021-09-06 12:07:28.801634'),
('eneb3p9w95rl1uqe25ujfdjco4xxm2qv', '.eJxVjEEOwiAQRe_C2pBCYYa6dO8ZyMBMpWpoUtqV8e7apAvd_vfef6lI21ri1mSJE6uzMur0uyXKD6k74DvV26zzXNdlSnpX9EGbvs4sz8vh_h0UauVbi_PedRggIEDqE3QmoVhvM2DAYQzZjWhYMDAxWSPiHXLqwXG2gIN6fwC_zjdl:1mLstf:dz7f-2VodawcE4fR8b7x33B0sQMVw9eysByYGrqQGOg', '2021-09-16 20:01:15.619267'),
('z5e8soyrw60mzohq4pfhvlkzx5sdgygp', '.eJxVjEEOwiAQRe_C2pBCYYa6dO8ZyMBMpWpoUtqV8e7apAvd_vfef6lI21ri1mSJE6uzMur0uyXKD6k74DvV26zzXNdlSnpX9EGbvs4sz8vh_h0UauVbi_PedRggIEDqE3QmoVhvM2DAYQzZjWhYMDAxWSPiHXLqwXG2gIN6fwC_zjdl:1mLNWI:CD-2tvjOj6W630GNnBbB0-wzvHpXNYobRI0diwxytE0', '2021-09-15 10:31:02.497747'),
('d0zi38olitt8h3u17lamvxk03cwlcyxa', '.eJxVjEEOwiAQRe_C2pBCYYa6dO8ZyMBMpWpoUtqV8e7apAvd_vfef6lI21ri1mSJE6uzMur0uyXKD6k74DvV26zzXNdlSnpX9EGbvs4sz8vh_h0UauVbi_PedRggIEDqE3QmoVhvM2DAYQzZjWhYMDAxWSPiHXLqwXG2gIN6fwC_zjdl:1mMS1h:UNn_I1mbMmwpe79boyBs8rOSpa6fCX1fXrZvZ9OcE5w', '2021-09-18 09:31:53.829904'),
('5x4crcaorsxiug2igzy0uj37rex8ovqh', '.eJxVjEEOwiAQRe_C2pBCYYa6dO8ZyMBMpWpoUtqV8e7apAvd_vfef6lI21ri1mSJE6uzMur0uyXKD6k74DvV26zzXNdlSnpX9EGbvs4sz8vh_h0UauVbi_PedRggIEDqE3QmoVhvM2DAYQzZjWhYMDAxWSPiHXLqwXG2gIN6fwC_zjdl:1mMT00:s8UfDZ37Gl0BczYPwnSKwYAwagARzKsdDqdSxfjfvqE', '2021-09-18 10:34:12.099588'),
('tyulqmhq5hin3rdyg85bavt0lqu425xn', '.eJxVjEEOwiAQRe_C2pBCYYa6dO8ZyMBMpWpoUtqV8e7apAvd_vfef6lI21ri1mSJE6uzMur0uyXKD6k74DvV26zzXNdlSnpX9EGbvs4sz8vh_h0UauVbi_PedRggIEDqE3QmoVhvM2DAYQzZjWhYMDAxWSPiHXLqwXG2gIN6fwC_zjdl:1mQ7Ho:E-doj2YZPNFPE8yRuxs1g0R8YWRk92Az5b_1FkZ_mDw', '2021-09-28 12:11:40.828095'),
('1e6mhr8nqp4jyp7rs2jgepyczh4kowj4', '.eJxVjEEOwiAQRe_C2pBCYYa6dO8ZyMBMpWpoUtqV8e7apAvd_vfef6lI21ri1mSJE6uzMur0uyXKD6k74DvV26zzXNdlSnpX9EGbvs4sz8vh_h0UauVbi_PedRggIEDqE3QmoVhvM2DAYQzZjWhYMDAxWSPiHXLqwXG2gIN6fwC_zjdl:1mQ9DR:J50jfNRz_YH50qqIStn4PiPsAbKHY2Fz1RCim6XlssI', '2021-09-28 14:15:17.730419'),
('f7vrueds9h6polrf7zmegegllca857bs', '.eJxVjEEOwiAQRe_C2pBCYYa6dO8ZyMBMpWpoUtqV8e7apAvd_vfef6lI21ri1mSJE6uzMur0uyXKD6k74DvV26zzXNdlSnpX9EGbvs4sz8vh_h0UauVbi_PedRggIEDqE3QmoVhvM2DAYQzZjWhYMDAxWSPiHXLqwXG2gIN6fwC_zjdl:1mQSi0:ZJth1bdx3fJJGA7XJY9PXnfPrqN8pxCA9v2vkVUjQGw', '2021-09-29 11:04:08.356719'),
('l0wjjjigly1nztdrbo6wb9z2qlv37mwt', '.eJxVjEEOwiAQRe_C2pBCYYa6dO8ZyMBMpWpoUtqV8e7apAvd_vfef6lI21ri1mSJE6uzMur0uyXKD6k74DvV26zzXNdlSnpX9EGbvs4sz8vh_h0UauVbi_PedRggIEDqE3QmoVhvM2DAYQzZjWhYMDAxWSPiHXLqwXG2gIN6fwC_zjdl:1mQTHe:kYVZB9tHO1O5MODxevCeDoNgH1Y-0ZTkHaQZ3cSPxa8', '2021-09-29 11:40:58.618200'),
('3lu0tqwy2p6xe4l0bz42nqvitnp1kha3', '.eJxVjEEOwiAQRe_C2pBCYYa6dO8ZyMBMpWpoUtqV8e7apAvd_vfef6lI21ri1mSJE6uzMur0uyXKD6k74DvV26zzXNdlSnpX9EGbvs4sz8vh_h0UauVbi_PedRggIEDqE3QmoVhvM2DAYQzZjWhYMDAxWSPiHXLqwXG2gIN6fwC_zjdl:1mQWuz:ilYpBo8czWIgRm6H9oLgKvGTsZTqdPQ2AA_rimaGoLU', '2021-09-29 15:33:49.647167'),
('s86u9zqplqqfw10z71vnn3ok2bvmxal2', '.eJxVjEEOwiAQRe_C2pBCYYa6dO8ZyMBMpWpoUtqV8e7apAvd_vfef6lI21ri1mSJE6uzMur0uyXKD6k74DvV26zzXNdlSnpX9EGbvs4sz8vh_h0UauVbi_PedRggIEDqE3QmoVhvM2DAYQzZjWhYMDAxWSPiHXLqwXG2gIN6fwC_zjdl:1mQWwq:pBae7zOVXBXKGP9vKRFhsN5X2gjLJmXdQpiiyMzvPgE', '2021-09-29 15:35:44.114878'),
('mopsedtyjvjepww2xhznurq1yicjl1fm', '.eJxVjEEOwiAQRe_C2pBCYYa6dO8ZyMBMpWpoUtqV8e7apAvd_vfef6lI21ri1mSJE6uzMur0uyXKD6k74DvV26zzXNdlSnpX9EGbvs4sz8vh_h0UauVbi_PedRggIEDqE3QmoVhvM2DAYQzZjWhYMDAxWSPiHXLqwXG2gIN6fwC_zjdl:1mQXfb:u0ACc-iov328Ukhl7lE4d1Bxb9sqw_OJVGmRBKe_dOY', '2021-09-29 16:21:59.712440'),
('rkbx7fn4psv8virmn0cc6zvsakro6mxr', '.eJxVjEEOwiAQRe_C2pBCYYa6dO8ZyMBMpWpoUtqV8e7apAvd_vfef6lI21ri1mSJE6uzMur0uyXKD6k74DvV26zzXNdlSnpX9EGbvs4sz8vh_h0UauVbi_PedRggIEDqE3QmoVhvM2DAYQzZjWhYMDAxWSPiHXLqwXG2gIN6fwC_zjdl:1mQjrT:XndQOIFN8vaH5omUWQ8EAsFMNbl8qdScYfqF45GQdP0', '2021-09-30 05:23:03.868335'),
('sj0v5ahmmcb64fk6x57n2vtf9ygppml7', '.eJxVjEEOwiAQRe_C2pBCYYa6dO8ZyMBMpWpoUtqV8e7apAvd_vfef6lI21ri1mSJE6uzMur0uyXKD6k74DvV26zzXNdlSnpX9EGbvs4sz8vh_h0UauVbi_PedRggIEDqE3QmoVhvM2DAYQzZjWhYMDAxWSPiHXLqwXG2gIN6fwC_zjdl:1mS4ex:R-tH39YgXSbErcCKpj-gVc5OzP6ALwZFQd10t9MeEUs', '2021-10-03 21:47:39.745615'),
('gly590jg7l8tt8ee0ysdw98cr11d7z35', '.eJxVjEEOwiAQRe_C2pBCYYa6dO8ZyMBMpWpoUtqV8e7apAvd_vfef6lI21ri1mSJE6uzMur0uyXKD6k74DvV26zzXNdlSnpX9EGbvs4sz8vh_h0UauVbi_PedRggIEDqE3QmoVhvM2DAYQzZjWhYMDAxWSPiHXLqwXG2gIN6fwC_zjdl:1mREQN:FSEezoR2w5V7sQHrhCSf8MN8JMqSyFoIMduGe4oN-vg', '2021-10-01 14:01:07.284882'),
('qcqoy0x2bdu4nv9p5vq7nt8wrfsbb0jv', '.eJxVjEEOwiAQRe_C2pBCYYa6dO8ZyMBMpWpoUtqV8e7apAvd_vfef6lI21ri1mSJE6uzMur0uyXKD6k74DvV26zzXNdlSnpX9EGbvs4sz8vh_h0UauVbi_PedRggIEDqE3QmoVhvM2DAYQzZjWhYMDAxWSPiHXLqwXG2gIN6fwC_zjdl:1mRFsn:ARbxuocDBuQM43jUd1Jixpw3t3p2o6urqWu5IyDYfWI', '2021-10-01 15:34:33.447235'),
('zz921oq63chkv59hw1ovuptaqfs7w3ul', '.eJxVjEEOwiAQRe_C2pBCYYa6dO8ZyMBMpWpoUtqV8e7apAvd_vfef6lI21ri1mSJE6uzMur0uyXKD6k74DvV26zzXNdlSnpX9EGbvs4sz8vh_h0UauVbi_PedRggIEDqE3QmoVhvM2DAYQzZjWhYMDAxWSPiHXLqwXG2gIN6fwC_zjdl:1mRekT:0xHLwG_dyfApa6VZ9TKremGBfFIC1hyNnHILAyXKRa4', '2021-10-02 18:07:37.232380'),
('sh0oz85ztmcy5y2p37mc9ipkntfynolv', '.eJxVjEEOwiAQRe_C2pBCYYa6dO8ZyMBMpWpoUtqV8e7apAvd_vfef6lI21ri1mSJE6uzMur0uyXKD6k74DvV26zzXNdlSnpX9EGbvs4sz8vh_h0UauVbi_PedRggIEDqE3QmoVhvM2DAYQzZjWhYMDAxWSPiHXLqwXG2gIN6fwC_zjdl:1mS5Kf:GbDSqm-SzJlVJ1bKQf9-hvMPcBsoThywidVLONvNs0w', '2021-10-03 22:30:45.148320'),
('q823wuryynavczee2i8yhwvs4nohax5f', '.eJxVjEEOwiAQRe_C2pBCYYa6dO8ZyMBMpWpoUtqV8e7apAvd_vfef6lI21ri1mSJE6uzMur0uyXKD6k74DvV26zzXNdlSnpX9EGbvs4sz8vh_h0UauVbi_PedRggIEDqE3QmoVhvM2DAYQzZjWhYMDAxWSPiHXLqwXG2gIN6fwC_zjdl:1mSEE5:bRaWLBqRGwcNGnkf97s8av7-exmZx2mnOLaCAuoC1DI', '2021-10-04 08:00:33.523780'),
('aiygia65c24t0l17cuc9lewps1403z4s', '.eJxVjEEOwiAQRe_C2pBCYYa6dO8ZyMBMpWpoUtqV8e7apAvd_vfef6lI21ri1mSJE6uzMur0uyXKD6k74DvV26zzXNdlSnpX9EGbvs4sz8vh_h0UauVbi_PedRggIEDqE3QmoVhvM2DAYQzZjWhYMDAxWSPiHXLqwXG2gIN6fwC_zjdl:1mSJLm:ZSf7unBlb3z-2hz_mf5Tocf_U02_MmtlrRbTvnQvhXg', '2021-10-04 13:28:50.777219'),
('pte4dql82hjl2tuf9xhu70bx3twa7f1j', '.eJxVjEEOwiAQRe_C2pBCYYa6dO8ZyMBMpWpoUtqV8e7apAvd_vfef6lI21ri1mSJE6uzMur0uyXKD6k74DvV26zzXNdlSnpX9EGbvs4sz8vh_h0UauVbi_PedRggIEDqE3QmoVhvM2DAYQzZjWhYMDAxWSPiHXLqwXG2gIN6fwC_zjdl:1mTdpY:hEvRVj1f6QqywXD_VLh7ufVuREv90WrL7T9mk0Wlt2c', '2021-10-08 05:33:04.763111'),
('4yzwm1vad2xu1fmf1uv82fugkmyujfm1', '.eJxVjEEOwiAQRe_C2pBCYYa6dO8ZyMBMpWpoUtqV8e7apAvd_vfef6lI21ri1mSJE6uzMur0uyXKD6k74DvV26zzXNdlSnpX9EGbvs4sz8vh_h0UauVbi_PedRggIEDqE3QmoVhvM2DAYQzZjWhYMDAxWSPiHXLqwXG2gIN6fwC_zjdl:1mVuCL:wzuk8emNO7XUTDMxP8lnhkMIPPPJzImu_bhOsHsebg0', '2021-10-14 11:25:57.248329'),
('1abpijluqd0980soxsqk2gcgcm5uy91q', '.eJxVjEEOwiAQRe_C2pBCYYa6dO8ZyMBMpWpoUtqV8e7apAvd_vfef6lI21ri1mSJE6uzMur0uyXKD6k74DvV26zzXNdlSnpX9EGbvs4sz8vh_h0UauVbi_PedRggIEDqE3QmoVhvM2DAYQzZjWhYMDAxWSPiHXLqwXG2gIN6fwC_zjdl:1mXO1j:w6-QNTWzMlQOuIKeRHKrW4V8bP3AUz3Zb04eM86qWnk', '2021-10-18 13:29:07.192976'),
('9rivyob95vvub9ekrkl0e7usq2nbu0ia', '.eJxVjEEOwiAQRe_C2pBCYYa6dO8ZyMBMpWpoUtqV8e7apAvd_vfef6lI21ri1mSJE6uzMur0uyXKD6k74DvV26zzXNdlSnpX9EGbvs4sz8vh_h0UauVbi_PedRggIEDqE3QmoVhvM2DAYQzZjWhYMDAxWSPiHXLqwXG2gIN6fwC_zjdl:1mXP9L:asX191o6xl1XtAoE5SABz69ivMx4c_1xbSe2gVEeVJI', '2021-10-18 14:41:03.116603'),
('0uqk49oukzqmqya2hedaznnvg1uruo07', '.eJxVjEEOwiAQRe_C2pBCYYa6dO8ZyMBMpWpoUtqV8e7apAvd_vfef6lI21ri1mSJE6uzMur0uyXKD6k74DvV26zzXNdlSnpX9EGbvs4sz8vh_h0UauVbi_PedRggIEDqE3QmoVhvM2DAYQzZjWhYMDAxWSPiHXLqwXG2gIN6fwC_zjdl:1mY6LK:oQMsu1PLqPtzqzam5rxlY7uZhretL4ofGWOdphToMMA', '2021-10-20 12:48:18.218941'),
('hijc9kaunjj7jhtgbjewdrdm0l1shgxh', '.eJxVjEEOwiAQRe_C2pBCYYa6dO8ZyMBMpWpoUtqV8e7apAvd_vfef6lI21ri1mSJE6uzMur0uyXKD6k74DvV26zzXNdlSnpX9EGbvs4sz8vh_h0UauVbi_PedRggIEDqE3QmoVhvM2DAYQzZjWhYMDAxWSPiHXLqwXG2gIN6fwC_zjdl:1mYTne:bKj9PXKb054Vm4znk7HoDSqUXBjAnDRmFRxpmwEC_uU', '2021-10-21 13:51:06.385134'),
('aga67pr17u5xr5jndcbslme1cz84oaib', '.eJxVjEEOwiAQRe_C2pBCYYa6dO8ZyMBMpWpoUtqV8e7apAvd_vfef6lI21ri1mSJE6uzMur0uyXKD6k74DvV26zzXNdlSnpX9EGbvs4sz8vh_h0UauVbi_PedRggIEDqE3QmoVhvM2DAYQzZjWhYMDAxWSPiHXLqwXG2gIN6fwC_zjdl:1ma0l3:dtkWJE3keMhD3mktrzDOPlclNJVBNMh37AzZZdsOQuk', '2021-10-25 19:14:45.496132'),
('8zpz6jb3fr8qb2jy80n27jd4r81pifqm', '.eJxVjEEOwiAQRe_C2pBCYYa6dO8ZyMBMpWpoUtqV8e7apAvd_vfef6lI21ri1mSJE6uzMur0uyXKD6k74DvV26zzXNdlSnpX9EGbvs4sz8vh_h0UauVbi_PedRggIEDqE3QmoVhvM2DAYQzZjWhYMDAxWSPiHXLqwXG2gIN6fwC_zjdl:1mYlaq:vEXNX4akvCoVbamprFRWw6-Mbx9ZqFJmehe8W2XaNtI', '2021-10-22 08:51:04.065440'),
('pav6gfctn4mnt50cgzehiwmlomcsyrol', '.eJxVjEEOwiAQRe_C2pBCYYa6dO8ZCMNMpWpoUtqV8e7apAvd_vfef6mYtrXErckSJ1ZnZdXpd6OUH1J3wPdUb7POc12XifSu6IM2fZ1ZnpfD_TsoqZVvLc5712GAgADUE3SGUKy3GTDgMIbsRjQsGDhxskbEO2TqwXG2gIN6fwDAZzdm:1mYmET:cZ6wgPZG2-tROICv0aTSSj0ls7GXm7FvmxkpVJnhVGw', '2021-10-22 09:32:01.658594'),
('h1hm33uavu6ojcm1hkiwy6eoob7w7300', '.eJxVjEEOwiAQRe_C2pBCYYa6dO8ZCMNMpWpoUtqV8e7apAvd_vfef6mYtrXErckSJ1ZnZdXpd6OUH1J3wPdUb7POc12XifSu6IM2fZ1ZnpfD_TsoqZVvLc5712GAgADUE3SGUKy3GTDgMIbsRjQsGDhxskbEO2TqwXG2gIN6fwDAZzdm:1ma0HT:E_KIJWdFv0KLlGUCBt5HXnMyPvgjepJq6y3W-WlfS7w', '2021-10-25 18:44:11.947045'),
('mxk35q9n97vmwnq78fwouadmaom5f94s', '.eJxVjEEOwiAQRe_C2pBCYYa6dO8ZyMBMpWpoUtqV8e7apAvd_vfef6lI21ri1mSJE6uzMur0uyXKD6k74DvV26zzXNdlSnpX9EGbvs4sz8vh_h0UauVbi_PedRggIEDqE3QmoVhvM2DAYQzZjWhYMDAxWSPiHXLqwXG2gIN6fwC_zjdl:1mhYiK:pD34wm0pGSiVhOt1CHSK4myWck0KZBm_52rH5Bv_TAo', '2021-11-15 14:55:08.797252'),
('9v9fwbms45kftvaok6fqs6nx5uevihmp', '.eJxVjEEOwiAQRe_C2pBCYYa6dO8ZyMBMpWpoUtqV8e7apAvd_vfef6lI21ri1mSJE6uzMur0uyXKD6k74DvV26zzXNdlSnpX9EGbvs4sz8vh_h0UauVbi_PedRggIEDqE3QmoVhvM2DAYQzZjWhYMDAxWSPiHXLqwXG2gIN6fwC_zjdl:1mlBke:bbfHeuC0t7YOYo-MlDRwVwshDUWWNnsOWEZVvu0AkWc', '2021-11-25 15:12:32.006301'),
('uvsj97mw3el8zkrugkbvkb0njg3m90cc', '.eJxVjEEOwiAQRe_C2pBCYYa6dO8ZyMBMpWpoUtqV8e7apAvd_vfef6lI21ri1mSJE6uzMur0uyXKD6k74DvV26zzXNdlSnpX9EGbvs4sz8vh_h0UauVbi_PedRggIEDqE3QmoVhvM2DAYQzZjWhYMDAxWSPiHXLqwXG2gIN6fwC_zjdl:1mmEmZ:ZffQFNnj2Qde7ffUsm2bKm22tPfW-UuahJU546jzH_I', '2021-11-28 12:38:51.387222'),
('dt6lm1yigudva8oozjlghsmpa7coenzb', '.eJxVjEEOwiAQRe_C2pBCYYa6dO8ZyMBMpWpoUtqV8e7apAvd_vfef6lI21ri1mSJE6uzMur0uyXKD6k74DvV26zzXNdlSnpX9EGbvs4sz8vh_h0UauVbi_PedRggIEDqE3QmoVhvM2DAYQzZjWhYMDAxWSPiHXLqwXG2gIN6fwC_zjdl:1mnfMg:u6Bm0rJZ-W1onmdsv9u-Qi5fv6xMmXDW7kDUMbg44TA', '2021-12-02 11:14:02.919570'),
('tosa0r3xx070r8fr6l976x02pvyuadkt', '.eJxVjEEOwiAQRe_C2pBCYYa6dO8ZyMBMpWpoUtqV8e7apAvd_vfef6lI21ri1mSJE6uzMur0uyXKD6k74DvV26zzXNdlSnpX9EGbvs4sz8vh_h0UauVbi_PedRggIEDqE3QmoVhvM2DAYQzZjWhYMDAxWSPiHXLqwXG2gIN6fwC_zjdl:1mnfNr:0oHklJk8F4bcZiEjIyzDsSxd4WQQX0HdAXOQ_0jiZe0', '2021-12-02 11:15:15.507614');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auth_group`
--
ALTER TABLE `auth_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  ADD KEY `auth_group_permissions_group_id_b120cbf9` (`group_id`),
  ADD KEY `auth_group_permissions_permission_id_84c5c92e` (`permission_id`);

--
-- Indexes for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`),
  ADD KEY `auth_permission_content_type_id_2f476e4b` (`content_type_id`);

--
-- Indexes for table `auth_user`
--
ALTER TABLE `auth_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  ADD KEY `auth_user_groups_user_id_6a12ed8b` (`user_id`),
  ADD KEY `auth_user_groups_group_id_97559544` (`group_id`);

--
-- Indexes for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  ADD KEY `auth_user_user_permissions_user_id_a95ead1b` (`user_id`),
  ADD KEY `auth_user_user_permissions_permission_id_1fbb5f2c` (`permission_id`);

--
-- Indexes for table `dashboard_activity`
--
ALTER TABLE `dashboard_activity`
  ADD PRIMARY KEY (`id`),
  ADD KEY `dashboard_activity_Slide_Id_id_8422ade1` (`Slide_Id_id`);

--
-- Indexes for table `dashboard_annotation`
--
ALTER TABLE `dashboard_annotation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `dashboard_annotation_Slide_Id_id_3458a1ec` (`Slide_Id_id`);

--
-- Indexes for table `dashboard_folder`
--
ALTER TABLE `dashboard_folder`
  ADD PRIMARY KEY (`id`),
  ADD KEY `dashboard_folder_Parent_id_15d85d30` (`Parent_id`);

--
-- Indexes for table `dashboard_sharedannotation`
--
ALTER TABLE `dashboard_sharedannotation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `dashboard_annotation_Slide_Id_id_3458a1ec` (`Slide_Id_id`);

--
-- Indexes for table `dashboard_slide`
--
ALTER TABLE `dashboard_slide`
  ADD PRIMARY KEY (`id`),
  ADD KEY `dashboard_slide_Folder_Id_id_634bc9a7` (`Folder_id`),
  ADD KEY `dashboard_slide_CopyOf_id_df6b512b` (`CopyOf_id`);

--
-- Indexes for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `django_admin_log_content_type_id_c4bce8eb` (`content_type_id`),
  ADD KEY `django_admin_log_user_id_c564eba6` (`user_id`);

--
-- Indexes for table `django_content_type`
--
ALTER TABLE `django_content_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`);

--
-- Indexes for table `django_migrations`
--
ALTER TABLE `django_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `django_session`
--
ALTER TABLE `django_session`
  ADD PRIMARY KEY (`session_key`),
  ADD KEY `django_session_expire_date_a5c62663` (`expire_date`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `auth_group`
--
ALTER TABLE `auth_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_permission`
--
ALTER TABLE `auth_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `auth_user`
--
ALTER TABLE `auth_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `dashboard_activity`
--
ALTER TABLE `dashboard_activity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `dashboard_annotation`
--
ALTER TABLE `dashboard_annotation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;

--
-- AUTO_INCREMENT for table `dashboard_folder`
--
ALTER TABLE `dashboard_folder`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=283;

--
-- AUTO_INCREMENT for table `dashboard_sharedannotation`
--
ALTER TABLE `dashboard_sharedannotation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `dashboard_slide`
--
ALTER TABLE `dashboard_slide`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=486;

--
-- AUTO_INCREMENT for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `django_content_type`
--
ALTER TABLE `django_content_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `django_migrations`
--
ALTER TABLE `django_migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
